<?php

/**
*  ReduxFramework Sample Config File
*  For full documentation, please visit: https://docs.reduxframework.com
 * */
if (!class_exists('Redux_Framework_WpOwl_config')) {

	class Redux_Framework_WpOwl_config {

		public $args        = array();
		public $sections    = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if (!class_exists('ReduxFramework')) {
				return;
			}

			// This is needed. Bah WordPress bugs.  ;)
			if (  true == Redux_Helpers::isTheme(__FILE__) ) {
				$this->initSettings();
			} else {
				add_action('plugins_loaded', array($this, 'initSettings'), 10);
			}

		}

		public function initSettings() {

			// Just for demo purposes. Not needed per say.
			//$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();

			if (!isset($this->args['opt_name'])) { // No errors please
				return;
			}

			// If Redux is running as a plugin, this will remove the demo notice and links
			add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

			// Function to test the compiler hook and demo CSS output.
			// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
			//add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);

			// Change the arguments after they've been declared, but before the panel is created
			//add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

			// Change the default value of a field after it's been set, but before it's been useds
			//add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

			// Dynamically add a section. Can be also used to modify sections/fields
			//add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
		}

		/**
		*
		*  This is a test function that will let you see when the compiler hook occurs.
		*  It only runs if a field	set with compiler=>true is changed.
		*
		 * */
		function compiler_action($options, $css) {
			//echo '<h1>The compiler hook has run!';
			//print_r($options); //Option values
			//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

			/*
			  // Demo of how to use the dynamic CSS and write your own static CSS file
			  $filename = dirname(__FILE__) . '/style' . '.css';
			  global $wp_filesystem;
			  if( empty( $wp_filesystem ) ) {
				require_once( ABSPATH .'/wp-admin/includes/file.php' );
			  WP_Filesystem();
			  }

			  if( $wp_filesystem ) {
				$wp_filesystem->put_contents(
					$filename,
					$css,
					FS_CHMOD_FILE // predefined mode settings for WP files
				);
			  }
			 */
		}

		/**
		*
		*  Custom function for filtering the sections array. Good for child themes to override or add to the sections.
		*  Simply include this function in the child themes functions.php file.
		*
		*  NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
		*  so you must use get_template_directory_uri() if you want to use any of the built in icons
		*
		 * */
		function dynamic_section($sections) {
			//$sections = array();
			$sections[] = array(
				'title' => __('Section via hook', 'wpowl-redux-framework'),
				'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'wpowl-redux-framework'),
				'icon' => 'el-icon-paper-clip',
				// Leave this as a blank section, no options just some intro text set above.
				'fields' => array()
			);

			return $sections;
		}

		/**
		*
		*  Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
		*
		 * */
		function change_arguments($args) {
			$args['dev_mode'] = false;

			return $args;
		}

		/**
		*
		*  Filter hook for filtering the default value of any given field. Very useful in development mode.
		*
		 * */
		function change_defaults($defaults) {
			$defaults['str_replace'] = 'Testing filter hook!';

			return $defaults;
		}

		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if (class_exists('ReduxFrameworkPlugin')) {
				remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

				// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
				remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
			}
		}

		public function setSections() {

			/**
			*  Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
			 * */
			// Background Patterns Reader
			$sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
			$sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
			$sample_patterns        = array();

			if (is_dir($sample_patterns_path)) :

				if ($sample_patterns_dir = opendir($sample_patterns_path)) :
					$sample_patterns = array();

					while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

						if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
							$name = explode('.', $sample_patterns_file);
							$name = str_replace('.' . end($name), '', $sample_patterns_file);
							$sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
						}
					}
				endif;
			endif;

			ob_start();

			$ct             = wp_get_theme();
			$this->theme    = $ct;
			$item_name      = $this->theme->get('Name');
			$tags           = $this->theme->Tags;
			$screenshot     = $this->theme->get_screenshot();
			$class          = $screenshot ? 'has-screenshot' : '';

			$customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'wpowl-redux-framework'), $this->theme->display('Name'));

			?>
			<div id="current-theme" class="<?php echo esc_attr($class); ?>">
			<?php if ($screenshot) : ?>
				<?php if (current_user_can('edit_theme_options')) : ?>
						<a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
							<img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
						</a>
				<?php endif; ?>
					<img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
				<?php endif; ?>

				<h4><?php echo $this->theme->display('Name'); ?></h4>

				<div>
					<ul class="theme-info">
						<li><?php printf(__('By %s', 'wpowl-redux-framework'), $this->theme->display('Author')); ?></li>
						<li><?php printf(__('Version %s', 'wpowl-redux-framework'), $this->theme->display('Version')); ?></li>
						<li><?php echo '<strong>' . __('Tags', 'wpowl-redux-framework') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
					</ul>
					<p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
			<?php
			if ($this->theme->parent()) {
				printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'wpowl-redux-framework'), $this->theme->parent()->display('Name'));
			}
			?>

				</div>
			</div>

			<?php
			$item_info = ob_get_contents();

			ob_end_clean();

			$sampleHTML = '';
			if (file_exists(dirname(__FILE__) . '/info-html.html')) {
				/** @global WP_Filesystem_Direct $wp_filesystem  */
				global $wp_filesystem;
				if (empty($wp_filesystem)) {
					require_once(ABSPATH . '/wp-admin/includes/file.php');
					WP_Filesystem();
				}
				$sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
			}

			// Get current Owl Carousel version
			$owl_js = file_get_contents( plugin_dir_path(__FILE__).'assets/owl-carousel/owl-carousel/owl.carousel.js' );
			$owl_ver = "<strong><em><span style='color:#fc0000;'>Unknown - please make sure
			that ".plugin_dir_url(__FILE__)."assets/owl-carousel/owl-carousel/owl.carousel.js exists.</span></em></strong>";
			if( preg_match('/v[0-9\.]{4,6}/', $owl_js, $match) ) $owl_ver = $match[0];

			// ACTUAL DECLARATION OF SECTIONS
			$this->sections[] = array(
				'title'     => __('General Settings', 'wpowl-redux-framework'),
				'desc'      => __('<p>WP Owl Carousel is created under the GPLv3 License and managed with love by <a href="http://caorda.com?source=plugin&name=WpOwlCarousel target="_blank">Caorda Web Solutions</a>.<br /></p><hr />
					<p><strong style="margin-right:30px;">Current Owl Carousel script version:</strong> <a href="'.plugin_dir_url(__FILE__).'assets/owl-carousel/owl-carousel/owl.carousel.js" target="_blank" title="View script">'.$owl_ver.'</a></p>
					<hr />', 'wpowl-redux-framework'),
				'icon'      => 'el-icon-home',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
						'id'        => 'opt-owl-console-debug',
						'type'      => 'switch',
						'title'     => __('Console Debug Mode', 'wpowl-redux-framework'),
						'desc'  	=> __('Prints plugin data to the developer console.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-text',
						'type'      => 'text',
						'title'     => __('Default jQuery Selector', 'wpowl-redux-framework'),
						'subtitle'  => __('Enables carousels on elements with this selector.', 'wpowl-redux-framework'),
						//'desc'      => __('This is the description field, again good for additional info.', 'wpowl-redux-framework'),
						'default'   => '.wp-owl-carousel',
					),
					array(
						'id'        => 'opt-override-wpgallery-checkbox',
						'type'      => 'switch',
						'title'     => __('Override default WordPress gallery shortcode', 'wpowl-redux-framework'),
						'desc'  => __('Adds handlers and options to the WordPress Gallery shortcode for easy insertion.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'Override Gallery Shortcode',
						'off'       => 'Don\'t Override',
					),
					array(
						'id'        => 'opt-global-enqueue-checkbox',
						'type'      => 'switch',
						'title'     => __('Globally Enqueue scripts', 'wpowl-redux-framework'),
						'desc'      => __('Adds scripts to each page regardless of whether the shortcode is present. Handy for making manual calls!', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'Global Enqueue',
						'off'       => 'Selective Enqueue',
					),
					array(
						'id'        => 'opt-enqueue-owl-checkbox',
						'type'      => 'switch',
						'title'     => __('Enqueue Owl Carousel Scripts', 'wpowl-redux-framework'),
						'desc'      => __('Adds the Owl Carousel scripts to pages that have carousels. This can be disabled if you are including the script elsewhere in your theme.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'Shortcode Enqueue',
						'off'       => 'Don\'t Enqueue',
					),
					array(
						'id'        => 'opt-minified-owl-checkbox',
						'type'      => 'switch',
						'title'     => __('Run minified Owl Js', 'wpowl-redux-framework'),
						'desc'      => __('Runs a smaller (minified) version of the carousel scripts, reducing page load.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'Production',
						'off'       => 'Development',
					),
					array(
						'id'        => 'opt-enqueue-fancybox-checkbox',
						'type'      => 'switch',
						'title'     => __('Enqueue Fancybox 3', 'wpowl-redux-framework'),
						'desc'      => __('Adds Fancybox 3 scripts to pages that have carousels. This can be disabled if you are including the script elsewhere in your theme..', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'Shortcode Enqueue',
						'off'       => 'Don\'t Enqueue',
					),
					array(
						'id'        => 'opt-enqueue-fancybox-thumbs-checkbox',
						'type'      => 'switch',
						'title'     => __('Enqueue Fancybox Thumbs Helper', 'wpowl-redux-framework'),
						'desc'      => __('Adds the Fancybox 3 thumbnail helper to your lightboxes.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'Shortcode Enqueue',
						'off'       => 'Don\'t Enqueue',
					),
					array(
						'id'        => 'opt-owl-baseClass',
						'type'      => 'text',
						'title'     => __('Base Class', 'wpowl-redux-framework'),
						'subtitle'  => __('baseClass', 'wpowl-redux-framework'),
						'desc'      => __('Automatically added class for base CSS styles. Do not add punctuation!', 'wpowl-redux-framework'),
						'default'      => 'owl-carousel',
					),
					array(
						'id'        => 'opt-owl-theme',
						'type'      => 'text',
						'title'     => __('Theme', 'wpowl-redux-framework'),
						'subtitle'  => __('theme', 'wpowl-redux-framework'),
						'desc'      => __('Default Owl CSS style for navigation and buttons. Do not add punctuation!', 'wpowl-redux-framework'),
						'default'   => 'owl-theme',
					),
				),
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			$this->sections[] = array(
				'icon'      => 'el-icon-globe',
				'title'     => __('Owl Carousel Basics', 'wpowl-redux-framework'),
				'fields'    => array(
					array(
						'id'            => 'opt-owl-items',
						'type'          => 'slider',
						'title'         => __('Visible Items', 'wpowl-redux-framework'),
						'subtitle'      => __('items', 'wpowl-redux-framework'),
						'desc'          => __('Determines how many items are visible by default', 'wpowl-redux-framework'),
						'default'       => 5,
						'min'           => 0,
						'step'          => 1,
						'max'           => 50,
						'display_value' => 'text'
					),
					array(
						'id'        => 'opt-owl-singleItem',
						'type'      => 'button_set',
						'title'     => __('Only Display Single Item', 'wpowl-redux-framework'),
						'subtitle' 	=> __('singleItem'),
						'desc'      => __('Force carousel to only display one item at a time.', 'wpowl-redux-framework'),
						'default'   => array('true'),
						'options' => array(
							'true'        => 'Single Item',
							'false'       => 'Many Items',
						),
					),
					array(
						'id'        => 'opt-owl-itemsScaleUp',
						'type'      => 'button_set',
						'title'     => __('Scale Images Up', 'wpowl-redux-framework'),
						'subtitle' 	=> __('itemsScaleUp'),
						'desc'      => __('Stretch images if they are smaller than the others.', 'wpowl-redux-framework'),
						'default'   => array('true'),
						'options'	=> array(
							'true'        => 'Scale images',
							'false'       => 'Do not scale images',
						),
					),
					array(
						'id'            => 'opt-owl-slideSpeed',
						'type'          => 'slider',
						'title'         => __('Slide Speed', 'wpowl-redux-framework'),
						'subtitle'      => __('slideSpeed', 'wpowl-redux-framework'),
						'desc'          => __('Determines how fast the slide transition executes in miliseconds', 'wpowl-redux-framework'),
						'default'       => 200,
						'min'           => 0,
						'step'          => 1,
						'max'           => 10000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-paginationSpeed',
						'type'          => 'slider',
						'title'         => __('Pagination Speed', 'wpowl-redux-framework'),
						'subtitle'      => __('paginationSpeed', 'wpowl-redux-framework'),
						'desc'          => __('Determines how fast the pagination transition executes in miliseconds', 'wpowl-redux-framework'),
						'default'       => 800,
						'min'           => 0,
						'step'          => 1,
						'max'           => 10000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-rewindSpeed',
						'type'          => 'slider',
						'title'         => __('Rewind Speed', 'wpowl-redux-framework'),
						'subtitle'      => __('rewindSpeed', 'wpowl-redux-framework'),
						'desc'          => __('Determines how fast the rewind transition executes in miliseconds', 'wpowl-redux-framework'),
						'default'       => 1000,
						'min'           => 0,
						'step'          => 1,
						'max'           => 10000,
						'display_value' => 'text'
					),
					array(
						'id'        => 'opt-owl-rewindNav',
						'type'      => 'switch',
						'title'     => __('Rewind Navigation', 'wpowl-redux-framework'),
						'subtitle' 	=> __('rewindNav'),
						'desc'      => __('Slides back to first item.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-autoPlay',
						'type'      => 'switch',
						'title'     => __('Autoplay Carousel', 'wpowl-redux-framework'),
						'subtitle' 	=> __('autoPlay'),
						'desc'      => __('Automatically starts rotating the carousel.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-scrollPerPage',
						'type'      => 'switch',
						'title'     => __('Scroll Per Page', 'wpowl-redux-framework'),
						'subtitle' 	=> __('scrollPerPage'),
						'desc'      => __('Moves the entire pagination length instead of by single item.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-autoHeight',
						'type'      => 'switch',
						'title'     => __('Auto Height', 'wpowl-redux-framework'),
						'subtitle' 	=> __('autoHeight'),
						'desc'      => __('Adjusts carousel to the height of the current image.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
				),
			);

			$this->sections[] = array(
				'icon'      => 'el-icon-align-justify',
				'title'     => __('Owl Carousel Nav', 'wpowl-redux-framework'),
				'fields'    => array(
					array(
						'id'        => 'opt-owl-navigation',
						'type'      => 'switch',
						'title'     => __('Navigation', 'wpowl-redux-framework'),
						'subtitle' 	=> __('navigation'),
						'desc'      => __('Displays previous/next buttons.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-navigationText',
						'type'      => 'text',
						'title'     => __('Navigation Text', 'wpowl-redux-framework'),
						'subtitle'  => __('navigationText', 'wpowl-redux-framework'),
						'desc'      => __('Sets the previous/next button text', 'wpowl-redux-framework'),
						//'required'	=> true,
						'options'      => array('prev'=>'Previous', 'next'=>'Next'),
						'default'	=> array('prev'=>'Previous', 'next'=>'Next'),
					),
					array(
						'id'        => 'opt-owl-pagination',
						'type'      => 'switch',
						'title'     => __('Pagination', 'wpowl-redux-framework'),
						'subtitle' 	=> __('pagination'),
						'desc'      => __('Shows pagination dots.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-paginationNumbers',
						'type'      => 'switch',
						'title'     => __('Pagination Numbers', 'wpowl-redux-framework'),
						'subtitle' 	=> __('paginationNumbers'),
						'desc'      => __('Displays numbers inside the pagination dots.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-responsive',
						'type'      => 'switch',
						'title'     => __('Responsive Behavior', 'wpowl-redux-framework'),
						'subtitle' 	=> __('responsive'),
						'desc'      => __('Makes the carousel responsive.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-stopOnHover',
						'type'      => 'switch',
						'title'     => __('Stop On Hover', 'wpowl-redux-framework'),
						'subtitle' 	=> __('stopOnHover'),
						'desc'      => __('Pauses transitions while the mouse is over the carousel.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-mouseDrag',
						'type'      => 'switch',
						'title'     => __('Enable Mouse Drag', 'wpowl-redux-framework'),
						'subtitle' 	=> __('mouseDrag'),
						'desc'      => __('Allows mouse drag events on carousel.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-touchDrag',
						'type'      => 'switch',
						'title'     => __('Enable Touch Drag', 'wpowl-redux-framework'),
						'subtitle' 	=> __('touchDrag'),
						'desc'      => __('Allows touch drag events on carousel.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),

				),
			);
			$this->sections[] = array(
				'icon'      => 'el-icon-cogs',
				'title'     => __('Owl Carousel Functionality', 'wpowl-redux-framework'),
				'fields'    => array(
					array(
						'id'        => 'opt-owl-lazyLoad',
						'type'      => 'switch',
						'title'     => __('Lazy Load', 'wpowl-redux-framework'),
						'subtitle' 	=> __('lazyLoad'),
						'desc'      => __('Delays loading of images.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-lazyFollow',
						'type'      => 'switch',
						'title'     => __('Lazy Follow', 'wpowl-redux-framework'),
						'subtitle' 	=> __('lazyFollow'),
						'desc'      => __('When pagination is used, it skips loading the images from pages that have been skipped.', 'wpowl-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-lazyEffect',
						'type'      => 'text',
						'title'     => __('Lazy Load Effect', 'wpowl-redux-framework'),
						'subtitle'  => __('lazyEffect', 'wpowl-redux-framework'),
						'desc'      => __('Effect that lazy-loaded images take on', 'wpowl-redux-framework'),
						'default'   => 'fade',
					),
					array(
						'id'        => 'opt-owl-jsonPath',
						'type'      => 'text',
						'title'     => __('JSON Path', 'wpowl-redux-framework'),
						'subtitle'  => __('jsonPath', 'wpowl-redux-framework'),
						'desc'      => __('Allows you to load directly from a JSON file', 'wpowl-redux-framework'),
						'default'   => '',
					),
					array(
						'id'        => 'opt-owl-dragBeforeAnimFinish',
						'type'      => 'switch',
						'title'     => __('Allow Dragging before Animation Finish', 'wpowl-redux-framework'),
						'subtitle' 	=> __('dragBeforeAnimFinish'),
						'desc'      => __('Let users drag carousel before the transition is complete.', 'wpowl-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-owl-addClassActive',
						'type'      => 'text',
						'title'     => __('Add Active Class', 'wpowl-redux-framework'),
						'subtitle'  => __('addClassActive', 'wpowl-redux-framework'),
						'desc'      => __('Adds this class to active carousel items', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-transitionStyle',
						'type'      => 'text',
						'title'     => __('CSS3 Transition Style', 'wpowl-redux-framework'),
						'subtitle'  => __('transitionStyle', 'wpowl-redux-framework'),
						'desc'      => __('Adds a CSS3 transition to the carousel item. Only works with single item carousels.', 'wpowl-redux-framework'),
						'default'	=> '',
					),
				),
			);

			$this->sections[] = array(
				'icon'      => 'el-icon-resize-full',
				'title'     => __('Owl Carousel Sizes', 'wpowl-redux-framework'),
				'fields'    => array(

					array(
						'id'        => 'section-itemscustom-checkbox',
						'type'      => 'switch',
						'title'     => __('Customize Items Layout', 'wpowl-redux-framework'),
						'desc'      => __('Allows custom layout specificaion (itemsCustom).', 'wpowl-redux-framework'),
						'default'   => false,
					),
					array(
						'id'        => 'section-itemscustom-start',
						'type'      => 'section',
						'title'     => __('Custom Items Layout', 'wpowl-redux-framework'),
						'subtitle'  => __('With the "section" field you can create indent option sections.', 'wpowl-redux-framework'),
						'indent'    => true,
						'required'  => array('section-itemscustom-checkbox', "=", 1),
					),
					array(
						'id'        => 'opt-owl-itemsCustom',
						'type'      => 'text',
						'url'       => true,
						'title'     => __('itemsCustom', 'wpowl-redux-framework'),
						'desc'      => __('This allows you to add custom variations of items depending from the width If this option is set, itemsDeskop, itemsDesktopSmall, itemsTablet, itemsMobile etc. are disabled For better preview, order the arrays by screen size, but it\'s not mandatory Don\'t forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available. Example: [[0, 2], [400, 4], [700, 6], [1000, 8], [1200, 10], [1600, 16]]', 'wpowl-redux-framework'),
						'default'   => '',
					),
					array(
						'id'        => 'section-itemscustom-end',
						'type'      => 'section',
						'indent'    => false, // Indent all options below until the next 'section' option is set.
						'required'  => array('section-itemscustom-checkbox', "=", 1),
					),
					array(
						'id'            => 'opt-owl-itemsDesktop-width',
						'type'          => 'slider',
						'title'         => __('Desktop Render Width', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsDesktop', 'wpowl-redux-framework'),
						'desc'          => __('Determines what width the Desktop mode is shown', 'wpowl-redux-framework'),
						'default'       => 1199,
						'min'           => 0,
						'step'          => 1,
						'max'           => 2000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsDesktop-items',
						'type'          => 'slider',
						'title'         => __('Desktop Width Items', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsDesktop', 'wpowl-redux-framework'),
						'desc'          => __('Determines how many items are visible at Desktop width', 'wpowl-redux-framework'),
						'default'       => 5,
						'min'           => 0,
						'step'          => 1,
						'max'           => 50,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsDesktopSmall-width',
						'type'          => 'slider',
						'title'         => __('Desktop Small Render Width', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsDesktop', 'wpowl-redux-framework'),
						'desc'          => __('Determines what width the Desktop Small mode is shown', 'wpowl-redux-framework'),
						'default'       => 980,
						'min'           => 0,
						'step'          => 1,
						'max'           => 2000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsDesktopSmall-items',
						'type'          => 'slider',
						'title'         => __('Desktop Small Width Items', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsDesktop', 'wpowl-redux-framework'),
						'desc'          => __('Determines how many items are visible at Desktop Small width', 'wpowl-redux-framework'),
						'default'       => 4,
						'min'           => 0,
						'step'          => 1,
						'max'           => 50,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsTablet-width',
						'type'          => 'slider',
						'title'         => __('Tablet Render Width', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsTablet', 'wpowl-redux-framework'),
						'desc'          => __('Determines what width the Tablet mode is shown', 'wpowl-redux-framework'),
						'default'       => 768,
						'min'           => 0,
						'step'          => 1,
						'max'           => 2000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsTablet-items',
						'type'          => 'slider',
						'title'         => __('Tablet Width Items', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsTablet', 'wpowl-redux-framework'),
						'desc'          => __('Determines how many items are visible at Tablet width', 'wpowl-redux-framework'),
						'default'       => 3,
						'min'           => 0,
						'step'          => 1,
						'max'           => 50,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsTabletSmall-width',
						'type'          => 'slider',
						'title'         => __('Tablet Small Render Width', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsTabletSmall', 'wpowl-redux-framework'),
						'desc'          => __('Determines what width the Tablet mode is shown', 'wpowl-redux-framework'),
						'default'       => 520,
						'min'           => 0,
						'step'          => 1,
						'max'           => 2000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsTabletSmall-items',
						'type'          => 'slider',
						'title'         => __('Tablet Small Width Items', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsTabletSmall', 'wpowl-redux-framework'),
						'desc'          => __('Determines how many items are visible at Tablet Small width', 'wpowl-redux-framework'),
						'default'       => 2,
						'min'           => 0,
						'step'          => 1,
						'max'           => 50,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsMobile-width',
						'type'          => 'slider',
						'title'         => __('Mobile Render Width', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsMobile', 'wpowl-redux-framework'),
						'desc'          => __('Determines what width the Mobile mode is shown', 'wpowl-redux-framework'),
						'default'       => 479,
						'min'           => 0,
						'step'          => 1,
						'max'           => 2000,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-itemsMobile-items',
						'type'          => 'slider',
						'title'         => __('Mobile Width Items', 'wpowl-redux-framework'),
						'subtitle'      => __('itemsMobile', 'wpowl-redux-framework'),
						'desc'          => __('Determines how many items are visible at Mobile width', 'wpowl-redux-framework'),
						'default'       => 1,
						'min'           => 0,
						'step'          => 1,
						'max'           => 50,
						'display_value' => 'text'
					),
					array(
						'id'            => 'opt-owl-responsiveRefreshRate',
						'type'          => 'slider',
						'title'         => __('Responsive Refresh Rate', 'wpowl-redux-framework'),
						'subtitle'      => __('responsiveRefreshRate', 'wpowl-redux-framework'),
						'desc'          => __('Check the window every interval for responsive actions', 'wpowl-redux-framework'),
						'default'       => 200,
						'min'           => 0,
						'step'          => 1,
						'max'           => 10000,
						'display_value' => 'text'
					),
					array(
						'id'        => 'opt-owl-responsiveBaseWidth',
						'type'      => 'text',
						'title'     => __('Responsive Base Width', 'wpowl-redux-framework'),
						'subtitle'  => __('responsiveBaseWidth', 'wpowl-redux-framework'),
						'desc'      => __('Checks this element for sizing changes', 'wpowl-redux-framework'),
						'default'	=> 'window',
					),

				)
			);

			$this->sections[] = array(
				'icon'      => 'el-icon-comment',
				'title'     => __('Owl Carousel Callbacks', 'wpowl-redux-framework'),
				'desc'		=> __('Enter the name of your callback function here - more info provided in the help tab at 
					the top right of the page.'),
				'fields'    => array(
					array(
						'id'        => 'opt-owl-jsonSuccess',
						'type'      => 'text',
						'title'     => __('JSON Success', 'wpowl-redux-framework'),
						'subtitle'  => __('jsonSuccess', 'wpowl-redux-framework'),
						'desc'      => __('Success callback for $.getJSON build into carousel', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-beforeUpdate',
						'type'      => 'text',
						'title'     => __('Before Update', 'wpowl-redux-framework'),
						'subtitle'  => __('beforeUpdate', 'wpowl-redux-framework'),
						'desc'      => __('Before responsive update callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-afterUpdate',
						'type'      => 'text',
						'title'     => __('After Update', 'wpowl-redux-framework'),
						'subtitle'  => __('afterUpdate', 'wpowl-redux-framework'),
						'desc'      => __('After responsive update callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-beforeInit',
						'type'      => 'text',
						'title'     => __('Before Init', 'wpowl-redux-framework'),
						'subtitle'  => __('beforeInit', 'wpowl-redux-framework'),
						'desc'      => __('Before initialization callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-afterInit',
						'type'      => 'text',
						'title'     => __('After Update', 'wpowl-redux-framework'),
						'subtitle'  => __('afterInit', 'wpowl-redux-framework'),
						'desc'      => __('After initialization callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-beforeMove',
						'type'      => 'text',
						'title'     => __('Before Move', 'wpowl-redux-framework'),
						'subtitle'  => __('beforeMove', 'wpowl-redux-framework'),
						'desc'      => __('Before carousel move callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-afterMove',
						'type'      => 'text',
						'title'     => __('After Move', 'wpowl-redux-framework'),
						'subtitle'  => __('afterMove', 'wpowl-redux-framework'),
						'desc'      => __('After carousel move callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-afterAction',
						'type'      => 'text',
						'title'     => __('After Action', 'wpowl-redux-framework'),
						'subtitle'  => __('afterAction', 'wpowl-redux-framework'),
						'desc'      => __('After startup, move and update callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-startDragging',
						'type'      => 'text',
						'title'     => __('Drag Start', 'wpowl-redux-framework'),
						'subtitle'  => __('startDragging', 'wpowl-redux-framework'),
						'desc'      => __('Called on drag start event', 'wpowl-redux-framework'),
						'default'	=> '',
					),
					array(
						'id'        => 'opt-owl-afterLazyLoad',
						'type'      => 'text',
						'title'     => __('After Lazy Load', 'wpowl-redux-framework'),
						'subtitle'  => __('afterLazyLoad', 'wpowl-redux-framework'),
						'desc'      => __('After lazyload callback', 'wpowl-redux-framework'),
						'default'	=> '',
					),
				),
			);

			$this->sections[] = array(
				'title'     => __('Owl Carousel Output Formatting', 'wpowl-redux-framework'),
				'desc'      => __('<p>These advanced output formatting options may help control the final display of your carousel items.</p>', 'wpowl-redux-framework'),
				'icon'      => 'el-icon-wrench',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(

					array(
						'id'        => 'opt-owl-image-render-mode',
						'type'      => 'button_set',
						'title'     => __('Image Render Mode', 'wpowl-redux-framework'),
						'subtitle'  => __('Decide how to display images', 'wpowl-redux-framework'),
						'desc'      => __('Determines whether items output an actual image or as a CSS background-image attribute.', 'wpowl-redux-framework'),
						'options'	=> array(
							'image' => __('Image Tag Output'),
							'background' => __('CSS background-image'),
						),
						'multi' => false,
						'default'   => 'image',
					),

					// Hidden background section

					array(
						'id'       => 'opt-owl-image-dimensions',
						'type'     => 'dimensions',
						'units'    => array('em','px','%'),
						'title'    => __('Dimensions (Width/Height) Option', 'redux-framework-demo'),
						'subtitle' => __('Determine sizing for image boxes. <strong>ONLY applies for background-image mode.</strong>', 'redux-framework-demo'),
						'desc'     => __('Set width and height for empty background-image containers.<br /><strong>NOTE:</strong>
							Unless you know the exact width of your items, leave the width element blank to make it fit
							the item container.', 'redux-framework-demo'),
						'default'  => array(
							'width'   => '',
							'height'  => '100'
						),
					),

					// / Hidden background section
				),
			);



			$this->sections[] = array(
				'icon'      => 'el-icon-website',
				'title'     => __('Fancybox 3 Defaults', 'wpowl-redux-framework'),
				'fields'    => array(

					array(
						'id'        => 'opt-custom-html',
						'type'      => 'raw',
						'title'     => __('Coming Soon', 'wpowl-redux-framework'),
						'subtitle'  => __('Slow and steady wins the race.', 'wpowl-redux-framework'),
						//'desc'      => __('Slow and steady wins the race!', 'wpowl-redux-framework'),
						//'validate'  => 'html',
					),
				)
			);



			$this->sections[] = array(
				'type' => 'divide',
			);

			$this->sections[] = array(
				'title'     => __('Import / Export', 'wpowl-redux-framework'),
				'desc'      => __('Import and Export your Redux Framework settings from file, text or URL.', 'wpowl-redux-framework'),
				'icon'      => 'el-icon-refresh',
				'fields'    => array(
					array(
						'id'            => 'opt-import-export',
						'type'          => 'import_export',
						'title'         => 'Import Export',
						'subtitle'      => 'Save and restore your Redux options',
						'full_width'    => false,
					),
				),
			);

			if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
				$tabs['docs'] = array(
					'icon'      => 'el-icon-book',
					'title'     => __('Documentation', 'wpowl-redux-framework'),
					'content'   => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
				);
			}
		}

		public function setHelpTabs() {

			// Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
			$this->args['help_tabs'][] = array(
				'id'        => 'redux-help-tab-1',
				'title'     => __('How To Callbacks', 'wpowl-redux-framework'),
				'content'   => __('<h3>Callbacks</h3>
					<p>To define a callback, enter the name of the function in the boxes here. Callbacks can be overridden
					on a per-carousel basis.</p>
					<p>To receive the callback for a function called <strong>myCustomCallback</strong>, enter the following function in a theme or plugin Javascript file:</p>
					<pre>
&lt;script type="text/javascript" &gt;
  function myCustomCallback(elem, carousel){
	// Perform script actions here
	console.log( elem, carousel);
  }
&lt;/script&gt;
					</pre>
					', 'wpowl-redux-framework')
			);

			$this->args['help_tabs'][] = array(
				'id'        => 'redux-help-tab-2',
				'title'     => __('Paired Carousels', 'wpowl-redux-framework'),
				'content'   => __('<h3>Paired Carousels</h3>
					<p>Carousels can be tied together so that one carouse acts as navigation for the other. A potential
					implementation might be to have a 5-image thumbnail carousel trigger a navigation event for a large-format
					single-image carousel.</p>
					<p>To pair multiple carousels, use the <strong>Carousel Name</strong> and <strong>Parent Carousel</strong>
					shortcode fields. The target (parent) carousel must have the <strong>Carousel Name</strong> shortcode field set,
					while the trigger (child) carousels must have the <strong>Parent Carousel</strong> field set to match that
					of the parent.</p>
					<h3>Example</h3>
					<h5>Parent (Target) Carousel</h5>
					<pre>[gallery wpowlcarousel_render_mode="wp_owl_carousel" carouselname="my_parent_carousel" items="1" singleitem="true" ids="1,2,3"]</pre>
					<h5>Child (Trigger) Carousel</h5>
					<pre>[gallery wpowlcarousel_render_mode="wp_owl_carousel" parentcarousel="my_parent_carousel" items="5" singleitem="false" ids="1,2,3"]</pre>
					<p><strong>IMPORTANT NOTE:</strong> Both carousels must contain the same image IDs in the same order! Navigation
					is calculated by using the numeric index of each item.</p>
					', 'wpowl-redux-framework')
			);
			$this->args['help_tabs'][] = array(
				'id'        => 'redux-help-tab-3',
				'title'     => __('Output Formatting', 'wpowl-redux-framework'),
				'content'   => __('<h3>Output Formatting</h3>
					<p>Depending on the uniform size, resizing and cropping of your images, you may find that some images
					in the carousel are shorter than others, resulting in a poorly-appearing carousel. If this is the case,
					you can try changing the <strong>Image Render Mode</strong> from "Image Tag Output" to "CSS Background-image".</p>
					<p>Making this change will render the carousel images as a <em>background-image</em> CSS property instead of 
					the default image tag. This means that you can vertically-align and resize these images instead of fighting
					with table display layouts, hopefully making it a bit easier to achieve a cleaner display.</p>
					<p><strong>NOTE:</strong> setting the width dimension for the image box will restrict the display of the
					container, but your carousel will probably auto-width the items anyway - for best results, leave this blank.</p>
					', 'wpowl-redux-framework')
			);


			// Set the help sidebar
			$this->args['help_sidebar'] = __('<p>Caorda Web Solutions does not offer direct support for this product.</p>', 'wpowl-redux-framework');
		}

		/**
		*
		*  All the possible arguments for Redux.
		*  For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		*
		 * */
		public function setArguments() {

			//$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
				// TYPICAL -> Change these values as you need/desire
				'opt_name'          => 'WpOwl_Settings',            // This is where your data is stored in the database and also becomes your global variable name.
				'display_name'      => 'WP Owl Carousel',     // Name that appears at the top of your panel
				'display_version'   => WP_OWL_CAROUSEL_VERSION,  // Version that appears at the top of your panel
				'menu_type'         => 'submenu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
				'menu_title'        => __('WP Owl Carousel', 'wpowl-redux-framework'),
				'page_title'        => __('WP Owl Carousel', 'wpowl-redux-framework'),

				// You will need to generate a Google API key to use this feature.
				// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
				'google_api_key' => '', // Must be defined to add google fonts to the typography module
				'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
				'admin_bar'         => false,                    // Show the panel pages on the admin bar
				'global_variable'   => 'WpOwl_Settings',                      // Set a different name for your global variable other than the opt_name
				'dev_mode'          => false,                    // Show the time the page took to load, etc
				'update_notice'		=> false,
				'customizer'        => false,                    // Enable basic customizer support

				// OPTIONAL -> Give you extra features
				'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
				'page_parent'       => 'options-general.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
				'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
				'menu_icon'         => plugins_url('img/owl-logo-16px.png', __FILE__),                      // Specify a custom URL to an icon
				'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
				'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
				'page_slug'         => 'wpowl_options',              // Page slug used to denote the panel
				'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
				'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
				'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
				'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.

				// CAREFUL -> These options are for advanced use only
				'transient_time'    => 60 * MINUTE_IN_SECONDS,
				'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
				'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
				'footer_credit'     => '<strong>WP Owl Carousel</strong> created by <a href="http://caorda.com?source=plugin&name=WpOwlCarousel" target="_blank">Caorda Web Solutions</a>',                   // Disable the footer credit of Redux. Please leave if you can help it.

				// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
				'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
				'system_info'           => false, // REMOVE

				// HINTS
				'hints' => array(
					'icon'          => 'icon-question-sign',
					'icon_position' => 'right',
					'icon_color'    => 'lightgray',
					'icon_size'     => 'normal',
					'tip_style'     => array(
						'color'         => 'light',
						'shadow'        => true,
						'rounded'       => false,
						'style'         => '',
					),
					'tip_position'  => array(
						'my' => 'top left',
						'at' => 'bottom right',
					),
					'tip_effect'    => array(
						'show'          => array(
							'effect'        => 'slide',
							'duration'      => '500',
							'event'         => 'mouseover',
						),
						'hide'      => array(
							'effect'    => 'slide',
							'duration'  => '500',
							'event'     => 'click mouseleave',
						),
					),
				)
			);


			// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
			/*
			$this->args['share_icons'][] = array(
				'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
				'title' => 'Visit us on GitHub',
				'icon'  => 'el-icon-github'
				//'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
			);
			*/
			$this->args['share_icons'][] = array(
				'url'   => 'https://www.facebook.com/caorda',
				'title' => 'Like us on Facebook',
				'icon'  => 'el-icon-facebook'
			);
			$this->args['share_icons'][] = array(
				'url'   => 'https://twitter.com/caorda',
				'title' => 'Follow us on Twitter',
				'icon'  => 'el-icon-twitter'
			);
			$this->args['share_icons'][] = array(
				'url'   => 'http://www.linkedin.com/company/caorda-solutions',
				'title' => 'Find us on LinkedIn',
				'icon'  => 'el-icon-linkedin'
			);

			// Panel Intro text -> before the form
			if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
				if (!empty($this->args['global_variable'])) {
					$v = $this->args['global_variable'];
				} else {
					$v = str_replace('-', '_', $this->args['opt_name']);
				}
				//$this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'wpowl-redux-framework'), $v);
			} else {
				//$this->args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'wpowl-redux-framework');
			}

			// Add content after the form.
			//$this->args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'wpowl-redux-framework');
		}

	}

	global $WpOwlreduxConfig;
	$WpOwlreduxConfig = new Redux_Framework_WpOwl_config();
}

/**
  *Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
	function redux_my_custom_field($field, $value) {
		//print_r($field);
		//echo '<br/>';
		//print_r($value);
	}
endif;

/**
  *Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
	function redux_validate_callback_function($field, $value, $existing_value) {
		$error = false;
		$value = 'just testing';

		/*
		  do your validation

		  if(something) {
			$value = $value;
		  } elseif(something else) {
			$error = true;
			$value = $existing_value;
			$field['msg'] = 'your custom error message';
		  }
		 */

		$return['value'] = $value;
		if ($error == true) {
			$return['error'] = $field;
		}
		return $return;
	}
endif;


function WpOwladdPanelCSS() {
	wp_register_style('wpowl-redux-custom-css', plugins_url('css/redux-custom.css', __FILE__), array( 'redux-css' ), time(), 'all' );
	wp_enqueue_style('wpowl-redux-custom-css');
}
// This example assumes your opt_name is set to redux_demo, replace with your opt_name value
add_action( 'redux/page/WpOwl_Settings/enqueue', 'WpOwladdPanelCSS' );