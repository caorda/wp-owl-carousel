jQuery(document).ready(function($){
	//console.log('Starting owl sliders!');


	$('.wp-owl-carousel').each(function(){
		var id = $(this).data('owl-id'),
			owlName = $(this).data('owl-name'),
			owlParent = $(this).data('owl-parent');


		// Init fancybox
		if( WpOwlFancyboxOptions[id].enablefancybox == true){
			$(this).find('a img').parent('a').fancybox( WpOwlFancyboxOptions[id] );
		}

		// Turn off image click events if necessary
		if( WpOwlFancyboxOptions[id].disableclicks == true){
			$(this).find('a img').parent('a').on('click', function(e){
				e.preventDefault();
				return false;
			})
		}

		// Enable parent carousel navigation
		if( owlParent != ''){
			$(this).find('a img').parent('a').on('click', function(){
				var index = $(this).parents('.owl-item').index(),
					numItems = parseInt( $('.wp-owl-carousel[data-owl-id="'+id+'"]').data('owlCarousel').options.items ),
					centerIndex = parseInt( index - (numItems / 2));

				// Navigate this
				$('.wp-owl-carousel[data-owl-id="'+id+'"]').data('owlCarousel').goTo(centerIndex);

				// Navigate parent
				$('.wp-owl-carousel[data-owl-name="'+owlParent+'"]').data('owlCarousel').goTo(index);
			});
		}




		// Init carousel
		$(this).owlCarousel( WpOwlCarouselOptions[id] );

		// Remove init class
		$(this).removeClass('init');

		//console.log( WpOwlCarouselOptions[id] );
		//console.log( WpOwlFancyboxOptions[id] );

	});


	/* WHAT A LOAD OF BULLSHIT fucking fancybox hurr durr
	$(window).on('load', function(){
		$('.wp-owl-carousel').each(function(){
			var id = $(this).data('owl-id');
			if( WpOwlFancyboxOptions[id].enablefancybox == true){
				console.log('FNAYBVCS');
				$(this).find('a img').parent('a').fancybox( WpOwlFancyboxOptions[id] );
			}
		});
	})
*/

});
