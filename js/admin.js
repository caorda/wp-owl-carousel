jQuery(document).ready(function($){

	$('.toggle-wp-owl-container').live('click', function(){
		var target = $(this).data('toggle-target'),
			$tglText = $(this).find('.tgltext');

		$(target).slideToggle('fast');
		if( $tglText.text() == 'Show') $tglText.text('Hide');
		else $tglText.text('Show');
	});


});
