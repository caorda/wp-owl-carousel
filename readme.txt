=== WP Owl Carousel ===
Contributors: Eric McNiece
Donate link:
Tags:
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

Adds the ability to generate Owl carousels using images or content

== Description ==

Adds the ability to generate Owl carousels using images or content

== Installation ==


== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision
