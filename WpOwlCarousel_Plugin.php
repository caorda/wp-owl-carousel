<?php


include_once('WpOwlCarousel_LifeCycle.php');

class WpOwlCarousel_Plugin extends WpOwlCarousel_LifeCycle {

	/**
	 * See: http://plugin.michael-simpson.com/?page_id=31
	 * @return array of option meta data.
	 */
	public function getOptionMetaData() {
		//  http://plugin.michael-simpson.com/?page_id=31
		return array(
			//'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
			//'JquerySelector' => array(__('Default jQuery Selector', 'wp-owl-carousel' , ".wp-owl-carousel")),
			//'OverrideWPGallery' => array(__('Override default WordPress gallery shortcode', 'wp-owl-carousel'), 'true', 'false'),
			//'GlobalEnqueue' => array(__('Globally Enqueue all scripts (include on every page)', 'wp-owl-carousel'), 'false', 'true'),
			//'EnqueueOwlJs' => array(__('Enqueue Owl Carousel Scripts', 'wp-owl-carousel'), 'true', 'false'),
			//'ScriptMinifiedOwl' => array(__('Run minified Owl Js (production mode)', 'wp-owl-carousel'), 'true', 'false'),
			//'EnqueueFancybox3' => array(__('Enqueue Fancybox 3', 'wp-owl-carousel'), 'true', 'false'),
			//'EnqueueFancyboxThumbs' => array(__('Enqueue Fancybox Thumbs Helper', 'wp-owl-carousel'), 'false', 'true'),


			/*'CanSeeSubmitData' => array(__('Can See Submission data', 'wp-owl-carousel'),
										'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone') */
		);
	}

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

	protected function initOptions() {
		$options = $this->getOptionMetaData();
		if (!empty($options)) {
			foreach ($options as $key => $arr) {
				if (is_array($arr) && count($arr > 1)) {
					$this->addOption($key, $arr[1]);
				}
			}
		}
	}

	public function getPluginDisplayName() {
		return 'WP Owl Carousel';
	}

	protected function getMainPluginFileName() {
		return 'wp-owl-carousel.php';
	}

	/**
	 * See: http://plugin.michael-simpson.com/?page_id=101
	 * Called by install() to create any database tables if needed.
	 * Best Practice:
	 * (1) Prefix all table names with $wpdb->prefix
	 * (2) make table names lower case only
	 * @return void
	 */
	protected function installDatabaseTables() {
		//        global $wpdb;
		//        $tableName = $this->prefixTableName('mytable');
		//        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
		//            `id` INTEGER NOT NULL");
	}

	/**
	 * See: http://plugin.michael-simpson.com/?page_id=101
	 * Drop plugin-created tables on uninstall.
	 * @return void
	 */
	protected function unInstallDatabaseTables() {
		//        global $wpdb;
		//        $tableName = $this->prefixTableName('mytable');
		//        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
	}


	/**
	 * Perform actions when upgrading from version X to version Y
	 * See: http://plugin.michael-simpson.com/?page_id=35
	 * @return void
	 */
	public function upgrade() {
	}

	public function addActionsAndFilters() {
		global $WpOwl_Settings;
		if(!isset($WpOwl_Settings)) $WpOwl_Settings = get_option('WpOwl_Settings', array());

		// Add options administration page
		// http://plugin.michael-simpson.com/?page_id=47
		//add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));
		add_action('wp_head', array(&$this, 'wpowl_init_settings_obj') );

		add_image_size('owl_huge', 3000,3000,true);
		add_image_size('owl_slider_large', 650,420,true);
		add_image_size('owl_slider_small', 85,64,true);

		// Example adding a script & style just for the options administration page
		// http://plugin.michael-simpson.com/?page_id=47
		//        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
		//            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
		//            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
		//        }
		add_action( 'admin_enqueue_scripts', array(&$this, 'wpowl_admin_enqueue_scripts' ) );

		// Add Actions & Filters
		// http://plugin.michael-simpson.com/?page_id=37


		// Adding scripts & styles to all pages
		// Examples:
		//        wp_enqueue_script('jquery');
		//        wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
		//        wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));

		// Owl carousel
		wp_enqueue_style('owl-carousel', plugins_url('/assets/owl-carousel/owl-carousel/owl.carousel.css', __FILE__));
		wp_enqueue_style('owl-theme', plugins_url('/assets/owl-carousel/owl-carousel/owl.theme.css', __FILE__));
		wp_enqueue_style('owl-transitions', plugins_url('/assets/owl-carousel/owl-carousel/owl.transitions.css', __FILE__));
		wp_enqueue_style('wpowl_custom', plugins_url('/css/owl.custom.css', __FILE__));

		// Fancybox
		wp_enqueue_style('fancybox3', plugins_url('/assets/fancybox3/jquery.fancybox.css', __FILE__));


		// Fancybox thumbs helper
		if( $WpOwl_Settings['opt-enqueue-fancybox-thumbs-checkbox']){
			wp_enqueue_style('fancybox-thumbs', plugins_url('/assets/fancybox3/jquery.fancybox-thumbs.css', __FILE__));
		}

		// Global script enqueue
		if( $WpOwl_Settings['opt-global-enqueue-checkbox']){
			wp_enqueue_script('fancybox3', plugins_url('assets/fancybox3/jquery.fancybox.js', __FILE__), array('jquery'), '3.0', true);

			if( $WpOwl_Settings['opt-enqueue-fancybox-thumbs-checkbox'])
				wp_enqueue_script('fancybox-thumbs', plugins_url('assets/fancybox3/jquery.fancybox-thumbs.js', __FILE__), array('jquery'), '3.0', true);
		} // global enqueue

		// Register short codes
		// http://plugin.michael-simpson.com/?page_id=39


		// Override stock WP gallery shortcode
		include_once('WpOwlCarousel_ShortCode_wpowlcarousel.php');
			$sc = new WpOwlCarousel_ShortCode_wpowlcarousel();
			$sc->register('wp-owl-carousel');

		if( $WpOwl_Settings['opt-override-wpgallery-checkbox']){
			remove_shortcode('gallery', 'gallery_shortcode');
			add_shortcode('gallery',  array(&$this, 'wpowl_gallery_shortcode') );

			// Inserting dropdown render option into WP Media Gallery interface
			add_action('print_media_templates', array(&$this, 'add_print_media_templates') );
		}





		// Register AJAX hooks
		// http://plugin.michael-simpson.com/?page_id=41
		add_filter( 'default_content', array(&$this, 'wpse83397_add_editor_default_content' ));




	}

	// Add init var to head
	public function wpowl_init_settings_obj(){ ?>
		<script type="text/javascript">var WpOwlCarouselOptions = {}, WpOwlFancyboxOptions = {};</script>
	<?php }



	public function wpowl_admin_enqueue_scripts(){
		add_editor_style( plugins_url('/css/admin.css', __FILE__) );
		wp_enqueue_style('wpowl_admin', plugins_url('/css/admin.css', __FILE__));
		wp_enqueue_script('wpowl_admin', plugins_url('/js/admin.js', __FILE__), array('jquery'));
	}


	// Override WP Gallery shortcode!
	public function wpowl_gallery_shortcode($attr) {
		$args = '';

		// Switch between custom gallery or Owl carousel
		if( isset($attr['wpowlcarousel_render_mode']) && $attr['wpowlcarousel_render_mode'] == 'wp_owl_carousel' ){
			// Owl Carousel
			foreach($attr as $k=>$at) $args .= ' '.$k.'="'.$at.'" ';

			$output = do_shortcode('[wp-owl-carousel '.$args.']');
			return $output;
		} else {
			// Default gallery
			require_once(ABSPATH.'/wp-includes/media.php');
			$output = gallery_shortcode($attr);
			return $output;
		}

	} // wpowl_gallery_shortcode



	public function wpse83397_add_editor_default_content( $content )
	{
	    if ( "your_post_type" !== get_current_screen()->post_type )
	        return $content;

	    return sprintf(
	        '<img src="%s" title="Placeholder" />', 
	        plugin_dir_path( __FILE__ ).'placeholder.png'
	    );
	}












	public function add_print_media_templates(){

		// define your backbone template;
		// the "tmpl-" prefix is required,
		// and your input field should have a name="" data-setting attribute
		// matching the shortcode name

		global $_wp_additional_image_sizes;
     	$sizes = array();
 		foreach( get_intermediate_image_sizes() as $s ){
 			$sizes[ $s ] = array( 0, 0 );
 			if( in_array( $s, array( 'thumbnail', 'medium', 'large' ) ) ){
 				$sizes[ $s ][0] = get_option( $s . '_size_w' );
 				$sizes[ $s ][1] = get_option( $s . '_size_h' );
 			}else{
 				if( isset( $_wp_additional_image_sizes ) && isset( $_wp_additional_image_sizes[ $s ] ) )
 					$sizes[ $s ] = array( $_wp_additional_image_sizes[ $s ]['width'], $_wp_additional_image_sizes[ $s ]['height'], );
 			}
 		}

		?>
		<script type="text/html" id="tmpl-wpowl-carousel-gallery-setting">
			<label class="setting">
				<span><?php _e('Render Mode'); ?></span>
				<select id="wp-owl-render-mode" name="" data-setting="wpowlcarousel_render_mode">
					<option value="default_wp"> Default WP Gallery </option>
					<option value="wp_owl_carousel"> WP Owl Carousel </option>
				</select>
			</label>

			<p>&nbsp;</p>
			<a href="#" id="toggle-wp-owl-settings" class="toggle-wp-owl-container button media-button button-primary button-large media-button-insert" data-toggle-target=".wp-owl-gallery-settings">
				<span class="tgltext">Show</span> WP Owl Carousel Settings</a>

			<div class="wp-owl-gallery-settings wp-owl-toggle-container">

				<h3><a href="http://www.owlgraphic.com/owlcarousel/" target="_blank">WP Owl Carousel Settings</a></h3>

				<label class="setting">
					<span><?php _e('Image Size'); ?></span>
					<select id="wp-owl-image-size" name="image_size" data-setting="image_size">
						<?php foreach( $sizes as $size => $atts ): ?>
							<option value="<?php echo $size ?>"><?php echo $size.' &nbsp;'.$atts[0].'x'.$atts[1]; ?></option>
						<?php endforeach; ?>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('Carousel Name'); ?></span>
					<input type="text" name="carouselname" data-setting="carouselname" />
				</label>
				<label class="setting">
					<span><?php _e('Parent Carousel'); ?></span>
					<input type="text" name="parentcarousel" data-setting="parentcarousel" />
				</label>
				<label class="setting">
					<span><?php _e('# Items'); ?></span>
					<input type="text" name="items" data-setting="items" placeholder="5" />
				</label>
				<label class="setting">
					<span><?php _e('itemsCustom'); ?></span>
					<input type="text" name="itemscustom" data-setting="itemscustom" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('itemsDesktop'); ?></span>
					<input type="text" name="itemsdesktop" data-setting="itemsdesktop" placeholder="{1199,4}" />
				</label>
				<label class="setting">
					<span><?php _e('itemsDesktopSmall'); ?></span>
					<input type="text" name="itemsdesktopsmall" data-setting="itemsdesktopsmall" placeholder="{980,3}" />
				</label>
				<label class="setting">
					<span><?php _e('itemsTablet'); ?></span>
					<input type="text" name="itemstablet" data-setting="itemstablet" placeholder="{768,2}" />
				</label>
				<label class="setting">
					<span><?php _e('itemsTabletSmall'); ?></span>
					<input type="text" name="itemstabletsmall" data-setting="itemstabletsmall" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('itemsMobile'); ?></span>
					<input type="text" name="itemsmobile" data-setting="itemsmobile" placeholder="{479,1}" />
				</label>
				<label class="setting">
					<span><?php _e('singleItem'); ?></span>
					<select name="singleitem" data-setting="singleitem">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('itemsScaleUp'); ?></span>
					<select name="itemsscaleup" data-setting="itemsscaleup">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('slideSpeed'); ?></span>
					<input type="text" name="slidespeed" data-setting="slidespeed" placeholder="200" />
				</label>
				<label class="setting">
					<span><?php _e('paginationSpeed'); ?></span>
					<input type="text" name="paginationspeed" data-setting="paginationspeed" placeholder="800" />
				</label>
				<label class="setting">
					<span><?php _e('rewindSpeed'); ?></span>
					<input type="text" name="rewindspeed" data-setting="rewindspeed" placeholder="1000" />
				</label>
				<label class="setting">
					<span><?php _e('autoPlay'); ?></span>
					<select name="autoplay" data-setting="autoplay">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('stopOnHover'); ?></span>
					<select name="stoponhover" data-setting="stoponhover">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('navigation'); ?></span>
					<select name="navigation" data-setting="navigation">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('navigationText'); ?></span>
					<input type="text" name="navigationtext" data-setting="navigationtext" placeholder="{'prev','next'}" />
				</label>

				<label class="setting">
					<span><?php _e('rewindNav'); ?></span>
					<select name="rewindnav" data-setting="rewindnav">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('scrollPerPage'); ?></span>
					<select name="scrollperpage" data-setting="scrollperpage">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('pagination'); ?></span>
					<select name="pagination" data-setting="pagination">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('paginationNumber'); ?></span>
					<select name="paginationnumber" data-setting="paginationnumber">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('responsive'); ?></span>
					<select name="responsive" data-setting="responsive">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>

				<label class="setting">
					<span><?php _e('responsiveRefreshRate'); ?></span>
					<input type="text" name="responsiverefreshrate" data-setting="responsiverefreshrate" placeholder="200" />
				</label>
				<label class="setting">
					<span><?php _e('responsiveBaseWidth'); ?></span>
					<input type="text" name="responsivebasewidth" data-setting="responsivebasewidth" placeholder="window" />
				</label>
				<label class="setting">
					<span><?php _e('baseClass'); ?></span>
					<input type="text" name="baseclass" data-setting="baseclass" placeholder="owl-carousel" />
				</label>
				<label class="setting">
					<span><?php _e('Theme'); ?></span>
					<input type="text" name="owl_theme" data-setting="owl_theme" placeholder="owl-theme" />
				</label>

				<label class="setting">
					<span><?php _e('lazyLoad'); ?></span>
					<select name="lazyload" data-setting="lazyload">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('lazyFollow'); ?></span>
					<select name="lazyfollow" data-setting="lazyfollow">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('lazyEffect'); ?></span>
					<input type="text" name="lazyeffect" data-setting="lazyeffect" placeholder="fade" />
				</label>

				<label class="setting">
					<span><?php _e('autoHeight'); ?></span>
					<select name="autoheight" data-setting="autoheight">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('jsonPath'); ?></span>
					<input type="text" name="jsonpath" data-setting="jsonpath" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('jsonSuccess'); ?></span>
					<input type="text" name="jsonsuccess" data-setting="jsonsuccess" placeholder="false" />
				</label>

				<label class="setting">
					<span><?php _e('dragBeforeAnimFinish'); ?></span>
					<select name="dragbeforeanimfinish" data-setting="dragbeforeanimfinish">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('mouseDrag'); ?></span>
					<select name="mousedrag" data-setting="mousedrag">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('touchDrag'); ?></span>
					<select name="touchdrag" data-setting="touchdrag">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('transitionStyle'); ?></span>
					<input type="text" name="transitionstyle" data-setting="transitionstyle" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('addClassActive'); ?></span>
					<select name="addclassactive" data-setting="addclassactive">
						<option value="true">True</option>
						<option value="false">False</option>
					</select>
				</label>

				<p>&nbsp;</p>
				<h3>Callbacks</h3>

				<label class="setting">
					<span><?php _e('beforeUpdate'); ?></span>
					<input type="text" name="beforeupdate" data-setting="beforeupdate" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('afterUpdate'); ?></span>
					<input type="text" name="afterupdate" data-setting="afterupdate" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('beforeInit'); ?></span>
					<input type="text" name="beforeinit" data-setting="beforeinit" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('afterInit'); ?></span>
					<input type="text" name="afterinit" data-setting="afterinit" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('beforeMove'); ?></span>
					<input type="text" name="beforemove" data-setting="beforemove" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('afterMove'); ?></span>
					<input type="text" name="aftermove" data-setting="aftermove" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('afterAction'); ?></span>
					<input type="text" name="afteraction" data-setting="afteraction" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('startDragging'); ?></span>
					<input type="text" name="startdragging" data-setting="startdragging" placeholder="false" />
				</label>
				<label class="setting">
					<span><?php _e('afterLazyLoad'); ?></span>
					<input type="text" name="afterlazyload" data-setting="afterlazyload" placeholder="false" />
				</label>


			</div>

			<p>&nbsp;</p>
			<a href="#" id="toggle-wp-fb-settings" class="toggle-wp-owl-container button media-button button-primary button-large media-button-insert" data-toggle-target=".wp-owl-fb-settings">
				<span class="tgltext">Show</span> WP Owl Fancybox Settings</a>

			<div class="wp-owl-fb-settings wp-owl-toggle-container">
				<label class="setting">
					<span><?php _e('Enable Fancybox on Carousel'); ?></span>
					<select name="enablefancybox" data-setting="enablefancybox">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('Disable image click events'); ?></span>
					<select name="disableclicks" data-setting="disableclicks">
						<option value="true">True</option>
						<option value="false" selected="selected">False</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('Fancybox Theme'); ?></span>
					<select id="wp-fb-theme" name="fb_theme" data-setting="fb_theme">
						<option value="light">Light</option>
						<option value="dark">Dark</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('Caption Position'); ?></span>
					<select name="fb_caption_pos" data-setting="fb_caption_pos">
						<option value="inside">Inside</option>
						<option value="outside">Outside</option>
						<option value="over">Over</option>
						<option value="float">Float</option>
					</select>
				</label>

				<label class="setting">
					<span><?php _e('openEffect'); ?></span>
					<select name="openeffect" data-setting="openeffect">
						<option value="fade">fade</option>
						<option value="drop">drop</option>
						<option value="elastic">elastic</option>
						<option value="none">none</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('closeEffect'); ?></span>
					<select name="closeeffect" data-setting="closeeffect">
						<option value="fade">fade</option>
						<option value="drop">drop</option>
						<option value="elastic">elastic</option>
						<option value="none">none</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('nextEffect'); ?></span>
					<select name="nexteffect" data-setting="nexteffect">
						<option value="fade">fade</option>
						<option value="drop">drop</option>
						<option value="elastic">elastic</option>
						<option value="none">none</option>
					</select>
				</label>
				<label class="setting">
					<span><?php _e('prevEffect'); ?></span>
					<select name="preveffect" data-setting="preveffect">
						<option value="fade">fade</option>
						<option value="drop">drop</option>
						<option value="elastic">elastic</option>
						<option value="none">none</option>
					</select>
				</label>

				<label class="setting">
					<span><?php _e('Custom Link Class'); ?></span>
					<input type="text" name="fb_link_class" data-setting="fb_link_class" placeholder="" />
				</label>

				<!-- // TODO: Add more Fancybox3 settings! -->
			</div>
		</script>

		<script>

		jQuery(document).ready(function(){

			// add your shortcode attribute and its default value to the
			// gallery settings list; $.extend should work as well...
			_.extend(wp.media.gallery.defaults, {
				y_custom_attr: 'default_wp'
			});

			// merge default gallery settings template with yours
			wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
				template: function(view){
					console.log( wp.media.template('wpowl-carousel-gallery-setting')(view) );
					return wp.media.template('gallery-settings')(view)
						+ wp.media.template('wpowl-carousel-gallery-setting')(view);
				}
			});

		});

		</script>
		<?php

	} // add_print_media_templates

} // class


function wp_owl_get_image_sizes( $size = '' ) {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $get_intermediate_image_sizes as $_size ) {

                if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

                        $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
                        $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
                        $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

                } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

                        $sizes[ $_size ] = array( 
                                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
                        );

                }

        }

        // Get only 1 size if found
        if ( $size ) {

                if( isset( $sizes[ $size ] ) ) {
                        return $sizes[ $size ];
                } else {
                        return false;
                }

        }

        return $sizes;
}