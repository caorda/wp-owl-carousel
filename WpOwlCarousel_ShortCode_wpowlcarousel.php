<?php
include_once('WpOwlCarousel_ShortCodeScriptLoader.php');

class WpOwlCarousel_ShortCode_wpowlcarousel extends WpOwlCarousel_ShortCodeScriptLoader {

	static $addedAlready = false;
	public function addScript() {
		if (!self::$addedAlready) {
			self::$addedAlready = true;

			global $WpOwl_Settings;
			if(!isset($WpOwl_Settings)) $WpOwl_Settings = get_option('WpOwl_Settings', array());

			// Fancybox 2
			if( $WpOwl_Settings['opt-enqueue-fancybox-checkbox']){
				wp_register_script('fancybox3', plugins_url('assets/fancybox3/jquery.fancybox.js', __FILE__), array('jquery'), '3.0', true);
				wp_print_scripts('fancybox3');
			} // if enqueue fancybox

			// Fancybox helpers
			if( $WpOwl_Settings['opt-enqueue-fancybox-thumbs-checkbox']){
				wp_register_script('fancybox-thumbs', plugins_url('assets/fancybox3/jquery.fancybox-thumbs.js', __FILE__), array('jquery'), '1.0', true);
				wp_print_scripts('fancybox-thumbs');
			}

			// Owl Carousel
			if( $WpOwl_Settings['opt-enqueue-owl-checkbox']){

				if( $WpOwl_Settings['opt-minified-owl-checkbox']){
					wp_register_script('owl-carousel', plugins_url('assets/owl-carousel/owl-carousel/owl.carousel.min.js', __FILE__), array('jquery'), '1.0', true);
				} else{
					wp_register_script('owl-carousel', plugins_url('assets/owl-carousel/owl-carousel/owl.carousel.js', __FILE__), array('jquery'), '1.0', true);
				}

				wp_register_script('start-owl', plugins_url('js/start_owl.js', __FILE__), array('jquery'), '1.0', true);
				wp_print_scripts('owl-carousel');
				wp_print_scripts('start-owl');

			} // if script enqueue enabled


		} // not added yet
	}

	public function handleShortcode($atts, $content) {
		extract( shortcode_atts( array( 		// Warning: not all arguments are used here!
		    'ids' 					=> 0,
		    'image_size' 			=> 'large',
		    'theme' 				=> '',	// gets overridden
		    'owl_theme' 			=> 'owl-theme',
		    'fb_theme' 				=> 'light',
		    'fb_link_class' 		=> '',
		    'caption' 				=> 'outside',
		    'openEffect' 			=> 'drop',
		    'closeEffect' 			=> 'drop',
		    'nextEffect' 			=> 'elastic',
		    'prevEffect' 			=> 'elastic',
		    'carouselname' 			=> null,
		    'parentcarousel'		=> null,
		    'extra_classes' 		=> '',
		    'navigationtext'		=> null,
		    'image_display_type' 	=> 'image',
		    'image_dimensions'		=> false,


		    // Swtiches carousel content source
		    'content_type' => 'ids',		// posts, ids, manual

		    // 'posts' content arg type
		    'posts_per_page'   => 5,
			'offset'           => 0,
			'category'         => '',
			'category_name'    => '',
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'include'          => '',
			'exclude'          => '',
			'meta_key'         => '',
			'meta_value'       => '',
			'post_type'        => 'post',
			'post_mime_type'   => '',
			'post_parent'      => '',
			'post_status'      => 'publish',
			'suppress_filters' => true,

			// Allow for custom taxonomy query
			'custom_tax'	   => null,
			'custom_tax_term'  => null,

			// Fields to draw from with 'posts' content type
			'post_fields'		=> 'featured_image, post_content, post_title',
	    ), $atts ) );

		global $WpOwlInstances;
		if(!isset($WpOwlInstances)) $WpOwlInstances = 0;
		$owlId = ++$WpOwlInstances;

		// Handle custom tax query
		if( $atts['custom_tax'] && $atts['custom_tax_term']){
			$atts[ $atts['custom_tax'] ] = $atts['custom_tax_term'];
		}

		// Override navigation text array and string update
		if(strpos($atts['navigationtext'], ',')){
			$atts['navigationText'] = explode(',', $atts['navigationtext']);
			foreach($atts['navigationText'] as &$text){
				if( strpos($text, '.png') || strpos($text, '.gif') || strpos($text, '.jpg'))
					$text = "<img src='".$text."' />";
			}
			unset($atts['navigationtext']);
		}

		// Compile settings
		$owlSettings = $this->generateOwlSettings($atts, $owlId);

		$fbSettings = $this->generateFbSettings($atts, $owlId);
		$opts = $owlSettings['settings'];
		$output = $content_output = '';

		if( $content_source == 'ids'){
			$ids = explode(',', $ids);

			foreach($ids as $k=>$id){
				$parity = ($k%2) ? 'even' : 'odd';
				$post = get_post($id);
				$src = wp_get_attachment_image_src($id, $image_size);
				$src_full = wp_get_attachment_image_src($id, 'owl_huge');

				if( $src[0]) $content_output .= '<div class="item item-content-type-ids item-'.$k.'  '.$parity.' ">
					<a href="'.$src_full[0].'" class="'.$fb_link_class.'" title="'.$post->post_excerpt.'" rel="fb-'.$owlId.'">
						<img src="'.$src[0].'" alt="'.$post->post_excerpt.'" />
					</a>
				</div>';
			}
		} else if( 'posts' == $content_type){

			$posts = get_posts($atts);

			foreach($posts as $k=>$post){
				$parity = ($k%2) ? 'even' : 'odd';
				$content_output .= '<div class="item item-content-type-posts item-'.$k.' '.$parity.' ">';

				$fields = array_map('trim', explode(',', $post_fields));
				if( is_array($fields)){
					foreach($fields as $field){

						// Fields can be space-separated to enclose in the same div
						if( strpos($field, ' ')) $extract_fields = explode(' ', $field);
						else $extract_fields = array($field);

						$content_output .= '<div class="owl-field field-'.$field.'">';

						foreach($extract_fields as $extf){
							switch($extf){
								case 'featured_image':
									$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $image_size );

									if( $image[0]){

										// Add dimension constraints if specified
										$bgsize = "";
										if( $image_dimensions){
											$imgsize = getimagesize($image[0]);
											$contsize = explode(',', $image_dimensions);
											if( ($imgsize[0] < $contsize[0]) || ($imgsize[1] < $contsize[1]) ){
												$bgsize = "background-size:initial;";
											}
										}

										// Display <img /> or <span> with background
										if( $image_display_type == 'image'){
											$content_output .= '<img class="size-'.$image_size.'" src="'.$image[0].'" alt="" />';
										} else if($image_display_type == 'background'){
											$content_output .= '<span class="img-bg" style="background-image:url('.$image[0].'); '.$bgsize.'"></span>';
										}
									} // if image exists
									break;

								default:
									if( $tmp = get_field($extf)) $content_output .= '<span class="extf-'.$extf.'">'.$tmp.'</span>';
									else $content_output .= '<span class="extf-'.$extf.'">'.$post->$extf.'</span>';

							} // switch

						} // foreach extracted field

						$content_output .= '</div>';
					} // foreach

				} // if $fields is array

				$content_output .= '</div>';
			} // foreach posts

		}

		// Applying final wrappers
		if($content_output){

			// Set up name / parent
			$owlName = isset($carouselname) ? $carouselname : '';
			$owlParent = isset($parentcarousel) ? $parentcarousel : '';

			$output = '<div id="wp-owl-'.$owlId.'" class="wp-owl-carousel '.$extra_classes.' '.$opts['theme'].'" data-owl-id="'.$owlId.'" data-owl-name="'.$owlName.'" data-owl-parent="'.$owlParent.'">';
			$output .= $content_output;
			$output .= '</div><!-- /#wp-owl'.$owlId.' -->';
			$output .= $owlSettings['script'];
			$output .= $fbSettings['script'];
		} else{
			$output = '<!-- #wp-owl'.$owlId.': No images present. -->';
		}

		// Final output!
		return $output;

	} // handleShortcode

	public function generateFbSettings($atts, $id){
		$setting_prefix = 'fb_';

		$settings = array(
			'gal_name' => '',
			'enablefancybox' => false,
			'disableclicks' => false,
			'caption' => array( 'type' => 'inside'),
			'openEffect' => 'drop',
			'closeEffect' => 'drop',
			'nextEffect'  => 'elastic',
			'prevEffect'  => 'elastic',
			'theme' => 'dark',
		);

		// Translate caption setting
		if( isset($atts['fb_caption_pos'])){
			$settings['caption']['type'] = $atts['fb_caption_pos'];
		}

		// Check to see if we have a specific app setting in the array
		foreach($settings as $k=>&$set){
			if( isset($atts[$setting_prefix.$k]) && !empty($atts[$setting_prefix.$k]) ){
				$set = $atts[$setting_prefix.$k];
			}
		}

		// Perform custom merge
		$settings = $this->custom_merge($settings, $atts);
		$output = $this->compile_args($settings, $id, 'WpOwlFancyboxOptions');

		return $output;

	} // generateFbSettings

	public function generateOwlSettings($atts, $id){
		global $WpOwl_Settings;
		if(!isset($WpOwl_Settings)) $WpOwl_Settings = get_option('WpOwl_Settings', array());

		$setting_prefix = 'owl_';

		// Clean and convert pseudo-array strings
		foreach($atts as &$at){
			if( is_string($at) && strpos($at, ',') && strpos($at, '}') ){
				$at = explode(',', trim($at, '{}'));
			}
		}

		$settings = array(
			'items' 			=> $WpOwl_Settings['opt-owl-items'],
			'itemsCustom' 		=> false,
			'itemsDesktop' 		=> array(1199,4),
			'itemsDesktopSmall' => array(980,3),
			'itemsTablet' 		=> array(768,2),
			'itemsTabletSmall' 	=> false,
			'itemsMobile' 		=> array(479,1),

			'singleItem' 		=> $this->bool_var($WpOwl_Settings['opt-owl-singleItem']),
			'itemsScaleUp' 		=> $WpOwl_Settings['opt-owl-itemsScaleUp'],
			'slideSpeed' 		=> $WpOwl_Settings['opt-owl-slideSpeed'],
			'paginationSpeed' 	=> $WpOwl_Settings['opt-owl-paginationSpeed'],
			'rewindSpeed'		=> $WpOwl_Settings['opt-owl-rewindSpeed'],

			'autoPlay' 			=> $this->bool_var($WpOwl_Settings['opt-owl-autoPlay']),
			'stopOnHover' 		=> $this->bool_var($WpOwl_Settings['opt-owl-stopOnHover']),
			'navigation' 		=> $this->bool_var($WpOwl_Settings['opt-owl-navigation']),

			'navigationText' 	=> $this->array_var($WpOwl_Settings['opt-owl-navigationText']),

			'rewindNav' 		=> $this->bool_var($WpOwl_Settings['opt-owl-rewindNav']),
			'scrollPerPage' 	=> $this->bool_var($WpOwl_Settings['opt-owl-scrollPerPage']),
			'pagination' 		=> $this->bool_var($WpOwl_Settings['opt-owl-pagination']),
			'paginationNumber' 	=> $this->bool_var($WpOwl_Settings['opt-owl-paginationNumbers']),
			'responsive' 		=> $this->bool_var($WpOwl_Settings['opt-owl-responsive']),

			'responsiveRefreshRate' => $WpOwl_Settings['opt-owl-responsiveRefreshRate'],
			'responsiveBaseWidth' 	=> $WpOwl_Settings['opt-owl-responsiveBaseWidth'],
			'baseClass' 			=> $WpOwl_Settings['opt-owl-baseClass'],
			'theme' 				=> $WpOwl_Settings['opt-owl-theme'],

			'lazyLoad' 			=> $this->bool_var($WpOwl_Settings['opt-owl-lazyLoad']),
			'lazyFollow' 		=> $this->bool_var($WpOwl_Settings['opt-owl-lazyFollow']),
			'lazyEffect' 		=> $WpOwl_Settings['opt-owl-lazyEffect'],

			'autoHeight' 			=> $this->bool_var($WpOwl_Settings['opt-owl-responsive']),
			'jsonPath' 				=> $this->string_var($WpOwl_Settings['opt-owl-jsonPath']),
			'jsonSuccess' 			=> $this->string_var($WpOwl_Settings['opt-owl-jsonSuccess']),
			'dragBeforeAnimFinish' 	=> $this->bool_var($WpOwl_Settings['opt-owl-dragBeforeAnimFinish']),
			'mouseDrag' 			=> $this->bool_var($WpOwl_Settings['opt-owl-mouseDrag']),
			'touchDrag' 			=> $this->bool_var($WpOwl_Settings['opt-owl-touchDrag']),

			'transitionStyle' 	=> $this->string_var($WpOwl_Settings['opt-owl-transitionStyle']),
			'addClassActive' 	=> $this->string_var($WpOwl_Settings['opt-owl-addClassActive']),
			'beforeUpdate' 		=> $this->string_var($WpOwl_Settings['opt-owl-beforeUpdate']),
			'afterUpdate' 		=> $this->string_var($WpOwl_Settings['opt-owl-afterUpdate']),
			'beforeInit' 		=> $this->string_var($WpOwl_Settings['opt-owl-beforeInit']),
			'afterInit' 		=> $this->string_var($WpOwl_Settings['opt-owl-afterInit']),
			'beforeMove' 		=> $this->string_var($WpOwl_Settings['opt-owl-beforeMove']),
			'afterMove' 		=> $this->string_var($WpOwl_Settings['opt-owl-afterMove']),
			'afterAction' 		=> $this->string_var($WpOwl_Settings['opt-owl-afterAction']),
			'startDragging' 	=> $this->string_var($WpOwl_Settings['opt-owl-startDragging']),
			'afterLazyLoad' 	=> $this->string_var($WpOwl_Settings['opt-owl-afterLazyLoad']),
		);

		// Check to see if we have a specific app setting in the array
		foreach($settings as $k=>&$set){
			if( isset($atts[$setting_prefix.$k]) && !empty($atts[$setting_prefix.$k]) ){
				$set = $atts[$setting_prefix.$k];
			}
		}

		// Perform custom merge
		$settings = $this->custom_merge($settings, $atts);
		$output = $this->compile_args($settings, $id, 'WpOwlCarouselOptions');

		return $output;
	}

	// Quick translate Redux to JS-friendly boolean text
	public function bool_var($var){
		return $var ? 'true' : 'false';
	}
	public function array_var($array){
		$out = array();
		foreach($array as $var) $out[] = $var;
		return $out;
	}
	public function string_var($var){
		if(!$var) return false;
		return $var;
	}
	public function sanitize($var){
		return str_replace('"', '\"', $var);
	}

	public function compile_args($settings, $id, $varname){

		global $WpOwl_Settings;
		if(!isset($WpOwl_Settings)) $WpOwl_Settings = get_option('WpOwl_Settings', array());

		$output = '<script type="text/javascript">'.$varname.'['.$id.'] = {';
		$count = 0;
		foreach($settings as $k=>$set){
			if($count++) $output .= ', ';
			if( ($set === true) || ($set === 'true') ) $set = 'true';
			else if(($set === false) || ($set === 'false') ) $set = 'false';
			else if(is_string($set)) $set = '"'.$set.'"';

			else if(is_array($set) ) {
				$keys = array_keys($set);
				if( !is_numeric($keys[0])){
					$set = '{'.$keys[0].' : "'.$this->sanitize($set[$keys[0]]).'" }';
				} else{
					if( is_string($set[0])) $set = '["'.implode('", "', $this->sanitize($set)).'"]';
					else $set = '['.implode(', ', $this->sanitize($set)).']';
				}

			}

			$output .= $k." : ".print_r($set, true);
		}
		$output .= '};';

		if( $WpOwl_Settings['opt-owl-console-debug'])
			$output .= ' console.log("WP Owl Carousel :: Debug", '.$varname.');';

		$output .= '</script>';

		return array('settings'=>$settings, 'script' => $output);

	} // wp_owl_compile_args

	public function custom_merge($arr1, $arr2){

		foreach($arr1 as $k=>&$set){
			$k_low = strtolower($k);

			if( isset($arr2[$k]) && !empty($arr2[$k]) ){
				$set = $arr2[$k];
			}
			if( isset($arr2[$k_low]) && !empty($arr2[$k_low]) ){
				$set = $arr2[$k_low];
			}
		}

		return $arr1;

	} // wp_owl_custom_merge

} // class
