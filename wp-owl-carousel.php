<?php
/*
   Plugin Name: WP Owl Carousel
   Plugin URI: http://wordpress.org/extend/plugins/wp-owl-carousel/
   Version: 1.0.1
   Author: Eric McNiece
   Description: Adds the ability to generate Owl carousels using images or content
   Text Domain: wp-owl-carousel
   License: GPLv3
  */

/*
    "WordPress Plugin Template" Copyright (C) 2014 Michael Simpson  (email : michael.d.simpson@gmail.com)

    This following part of this file is part of WordPress Plugin Template for WordPress.

    WordPress Plugin Template is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordPress Plugin Template is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Contact Form to Database Extension.
    If not, see http://www.gnu.org/licenses/gpl-3.0.html
*/

$WpOwlCarousel_minimalRequiredPhpVersion = '5.0';
define("WP_OWL_CAROUSEL_VERSION", "1.0.1");

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function WpOwlCarousel_noticePhpVersionWrong() {
    global $WpOwlCarousel_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
      __('Error: plugin "WP Owl Carousel" requires a newer version of PHP to be running.',  'wp-owl-carousel').
            '<br/>' . __('Minimal version of PHP required: ', 'wp-owl-carousel') . '<strong>' . $WpOwlCarousel_minimalRequiredPhpVersion . '</strong>' .
            '<br/>' . __('Your server\'s PHP version: ', 'wp-owl-carousel') . '<strong>' . phpversion() . '</strong>' .
         '</div>';
}


function WpOwlCarousel_PhpVersionCheck() {
    global $WpOwlCarousel_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $WpOwlCarousel_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'WpOwlCarousel_noticePhpVersionWrong');
        return false;
    }
    return true;
}


/**
 * Initialize internationalization (i18n) for this plugin.
 * References:
 *      http://codex.wordpress.org/I18n_for_WordPress_Developers
 *      http://www.wdmac.com/how-to-create-a-po-language-translation#more-631
 * @return void
 */
function WpOwlCarousel_i18n_init() {
    $pluginDir = dirname(plugin_basename(__FILE__));
    load_plugin_textdomain('wp-owl-carousel', false, $pluginDir . '/languages/');
}


//////////////////////////////////
// Run initialization
/////////////////////////////////

// First initialize i18n
WpOwlCarousel_i18n_init();


// Next, run the version check.
// If it is successful, continue with initialization for this plugin
if (WpOwlCarousel_PhpVersionCheck()) {

  if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' );
  }
  if ( file_exists( dirname( __FILE__ ) . '/WpOwlCarousel_ReduxInit.php' ) ) {
      require_once( dirname( __FILE__ ) . '/WpOwlCarousel_ReduxInit.php' );
  }


    // Only load and run the init function if we know PHP version can parse it
    include_once('wp-owl-carousel_init.php');
    WpOwlCarousel_init(__FILE__);
}
